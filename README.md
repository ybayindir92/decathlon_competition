#Decathlon competition.

This challenge is about Decathlon competition.
 
Explanation and formulas can be found here: http://en.wikipedia.org/wiki/Decathlon

#####Requirements:
The input of the Java program is a CSV file. 
Each line must contain name of athlete and all ten game results.
Game performance times should be separated with period.
_Default file can be found at sample/results.csv_


The program should output an XML (to standard output) with all athletes in ascending order of their places, containing all the input data plus total score and the place in the competition (in case of equal scores, athletes must share the places, e.g. 3-4 and 3-4 instead of 3 and 4).

* The input and output file names should be provided as parameters to the Java application at the startup.
* Unit tests must cover the implementation.
* No external libraries are allowed except JUnit and Mockito.
* Code design should be simple yet flexible allowing easy modifications (e.g. new input/output formats)

_PS:The project was implemented a while ago._