package com.yasemin.decathlon.model;

import org.junit.Test;

import static com.yasemin.decathlon.model.Event.*;
import static com.yasemin.decathlon.model.EventType.FIELD;
import static com.yasemin.decathlon.model.EventType.TRACK;
import static org.junit.Assert.assertEquals;

public class EventTest {

    @Test
    public void shouldSetColumnsForSprint100m() {
        assertEquals(SPRINT_100M.getEventType(), TRACK);
        assertEquals(SPRINT_100M.getColumnA(), 25.4347f, 0.0f);
        assertEquals(SPRINT_100M.getColumnB(), 18f, 0.0f);
        assertEquals(SPRINT_100M.getColumnC(), 1.81f, 0.0f);
        assertEquals(SPRINT_100M.getUnitConstant(), 1);
    }

    @Test
    public void shouldSetColumnsForLongJump() {
        assertEquals(LONG_JUMP.getEventType(), FIELD);
        assertEquals(LONG_JUMP.getColumnA(), 0.14354f, 0.0f);
        assertEquals(LONG_JUMP.getColumnB(), 220f, 0.0f);
        assertEquals(LONG_JUMP.getColumnC(), 1.4f, 0.0f);
        assertEquals(LONG_JUMP.getUnitConstant(), 100);
    }


    @Test
    public void shouldSetColumnsForShotPut() {
        assertEquals(SHOT_PUT.getEventType(), FIELD);
        assertEquals(SHOT_PUT.getColumnA(), 51.39f, 0.0f);
        assertEquals(SHOT_PUT.getColumnB(), 1.5f, 0.0f);
        assertEquals(SHOT_PUT.getColumnC(), 1.05f, 0.0f);
        assertEquals(SHOT_PUT.getUnitConstant(), 1);
    }

    @Test
    public void shouldSetColumnsForHighJump() {
        assertEquals(HIGH_JUMP.getEventType(), FIELD);
        assertEquals(HIGH_JUMP.getColumnA(), 0.8465f, 0.0f);
        assertEquals(HIGH_JUMP.getColumnB(), 75f, 0.0f);
        assertEquals(HIGH_JUMP.getColumnC(), 1.42f, 0.0f);
        assertEquals(HIGH_JUMP.getUnitConstant(), 100);
    }

    @Test
    public void shouldSetColumnsForSprint400m() {
        assertEquals(SPRINT_400M.getEventType(), TRACK);
        assertEquals(SPRINT_400M.getColumnA(), 1.53775f, 0.0f);
        assertEquals(SPRINT_400M.getColumnB(), 82f, 0.0f);
        assertEquals(SPRINT_400M.getColumnC(), 1.81f, 0.0f);
        assertEquals(SPRINT_400M.getUnitConstant(), 1);
    }

    @Test
    public void shouldSetColumnsForHurdles110m() {
        assertEquals(HURDLES_110M.getEventType(), TRACK);
        assertEquals(HURDLES_110M.getColumnA(), 5.74352f, 0.0f);
        assertEquals(HURDLES_110M.getColumnB(), 28.5f, 0.0f);
        assertEquals(HURDLES_110M.getColumnC(), 1.92f, 0.0f);
        assertEquals(HURDLES_110M.getUnitConstant(), 1);
    }

    @Test
    public void shouldSetColumnsForDiscussThrow() {
        assertEquals(DISCUS_THROW.getEventType(), FIELD);
        assertEquals(DISCUS_THROW.getColumnA(), 12.91f, 0.0f);
        assertEquals(DISCUS_THROW.getColumnB(), 4.0f, 0.0f);
        assertEquals(DISCUS_THROW.getColumnC(), 1.1f, 0.0f);
        assertEquals(DISCUS_THROW.getUnitConstant(), 1);
    }

    @Test
    public void shouldSetColumnsForPoleVault() {
        assertEquals(POLE_VAULT.getEventType(), FIELD);
        assertEquals(POLE_VAULT.getColumnA(), 0.2797f, 0.0f);
        assertEquals(POLE_VAULT.getColumnB(), 100f, 0.0f);
        assertEquals(POLE_VAULT.getColumnC(), 1.35f, 0.0f);
        assertEquals(POLE_VAULT.getUnitConstant(), 100);
    }


    @Test
    public void shouldSetColumnsForJavelinThrow() {
        assertEquals(JAVELIN_THROW.getEventType(), FIELD);
        assertEquals(JAVELIN_THROW.getColumnA(), 10.14f, 0.0f);
        assertEquals(JAVELIN_THROW.getColumnB(), 7f, 0.0f);
        assertEquals(JAVELIN_THROW.getColumnC(), 1.08f, 0.0f);
        assertEquals(JAVELIN_THROW.getUnitConstant(), 1);
    }

    @Test
    public void shouldSetColumnsForSprint1500m() {
        assertEquals(SPRINT_1500M.getEventType(), TRACK);
        assertEquals(SPRINT_1500M.getColumnA(), 0.03768f, 0.0f);
        assertEquals(SPRINT_1500M.getColumnB(), 480f, 0.0f);
        assertEquals(SPRINT_1500M.getColumnC(), 1.85f, 0.0f);
        assertEquals(SPRINT_1500M.getUnitConstant(), 1);
    }

}