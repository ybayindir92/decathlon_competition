package com.yasemin.decathlon.model;

import org.junit.Test;

import static com.yasemin.decathlon.data.AthleteCreator.createAthleteWithNameAndScore;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AthleteTest {

    @Test
    public void shouldReturnPositiveIfScoreGrater() {
        // given
        Athlete yasemin = createAthleteWithNameAndScore("Yasemin", 4200);
        Athlete penny = createAthleteWithNameAndScore("Penny", 3500);

        // when
        final int actual = yasemin.compareTo(penny);

        // then
        assertTrue(actual > 0);
    }

    @Test
    public void shouldReturnNegativeIfScoreSmaller() {
        // given
        Athlete yasemin = createAthleteWithNameAndScore("Yasemin", 4200);
        Athlete penny = createAthleteWithNameAndScore("Penny", 3500);

        // when
        final int actual = penny.compareTo(yasemin);

        // then
        assertTrue(actual < 0);
    }

    @Test
    public void shouldCompareNamesIfScoresEqual() {
        // given
        Athlete alex = createAthleteWithNameAndScore("Alex", 4200);
        Athlete john = createAthleteWithNameAndScore("John", 3500);

        // when
        final int actual = alex.compareTo(john);

        // then
        assertTrue(actual > 0);
    }

    @Test
    public void shouldReturnZeroIfNamesAndScoresSame() {
        // given
        Athlete athlete = createAthleteWithNameAndScore("Alex", 4200);
        Athlete athleteOther = createAthleteWithNameAndScore("Alex", 4200);

        // when
        final int actual = athlete.compareTo(athleteOther);

        // then
        assertEquals(actual, 0);
    }

}