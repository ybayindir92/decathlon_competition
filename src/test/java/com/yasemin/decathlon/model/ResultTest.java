package com.yasemin.decathlon.model;

import com.yasemin.decathlon.bussiness.FieldEventCalculation;
import com.yasemin.decathlon.bussiness.TrackEventCalculation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyFloat;

@RunWith(MockitoJUnitRunner.class)
public class ResultTest {

    @Mock
    private TrackEventCalculation track;

    @Mock
    private FieldEventCalculation field;

    @InjectMocks
    private Result result;

    @Test
    public void shouldCalculateTotalScore() {
        // given
        given(field.calculateScore(any(Event.class), anyFloat())).willReturn(1);
        given(track.calculateScore(any(Event.class), anyFloat())).willReturn(2);

        // when
        final int actualScore = result.getTotalScore();

        // then
        assertEquals(actualScore, 14);
    }

}
