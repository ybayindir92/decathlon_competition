package com.yasemin.decathlon.document.parser;

import com.yasemin.decathlon.exception.DecathlonException;
import com.yasemin.decathlon.model.Athlete;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CSVAthleteParserTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private DocumentParser<Athlete> documentParser = new CSVAthleteParser();
    private List<Athlete> athletes = new ArrayList<>();



    @Test
    public void shouldParseDocument() throws IOException {
        // given
        String content = "John Smith;12.61;5.00;9.22;1.50;60.39;16.43;21.60;2.60;35.81;5.25.72"
                + System.lineSeparator()
                + "Jane Doe;13.04;4.53;7.79;1.55;64.72;18.74;24.20;2.40;28.20;6.50.76";
        File file = createTempFile(content);

        // when
        athletes = documentParser.parseDocument(file.getAbsolutePath());

        // then
        assertEquals(athletes.size(), 2);
    }


    @Test(expected = DecathlonException.class)
    public void shouldThrowDecathlonExceptionIfFileHasMissingData() throws IOException {
        // given
        File file = createTempFile("Sarah,1,2,3,4,5");

        // when
        athletes = documentParser.parseDocument(file.getAbsolutePath());
    }

    @Test(expected = IOException.class)
    public void shouldThrowExceptionIfFileNotExist() throws IOException {
        // given
        String fileName = "unknown.csv";

        // when
        athletes = documentParser.parseDocument(fileName);
    }

    private File createTempFile(final String content) throws IOException {
        File file = temporaryFolder.newFile("results.csv");
        FileWriter writer = new FileWriter(file);
        writer.write(content);
        writer.close();
        return file;
    }

}