package com.yasemin.decathlon.document.writer;

import com.yasemin.decathlon.model.Athlete;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import static com.yasemin.decathlon.data.AthleteCreator.createAthleteWithNameAndScore;
import static java.util.Objects.requireNonNull;
import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;
import static org.junit.Assert.assertTrue;

public class XMLAthleteWriterTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private final DocumentWriter<Athlete> xmlWriter = new XMLAthleteWriter();

    @Test
    public void shouldWriteXmlOutputWithCorrectFormat() throws Exception {
        // given
        List<Athlete> athletes = Collections.singletonList(createAthleteWithNameAndScore("Alan", 3000));
        final File file = temporaryFolder.newFile("out.xml");
        String outputPath = file.getAbsolutePath();

        // when
        xmlWriter.write(athletes, outputPath);

        //then
        assertTrue(file.exists());
        assertTrue(file.length() > 0);
        getValidator().validate(new StreamSource(new FileReader(outputPath)));
    }

    private Validator getValidator() throws SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
        final URL schemaUrl = requireNonNull(getClass().getClassLoader().getResource("schema.xsd"));
        Schema schema = factory.newSchema(schemaUrl);;
        return schema.newValidator();
    }

}