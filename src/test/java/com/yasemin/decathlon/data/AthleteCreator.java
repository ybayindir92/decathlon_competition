package com.yasemin.decathlon.data;

import com.yasemin.decathlon.model.Athlete;
import com.yasemin.decathlon.model.Result;

public class AthleteCreator {

    public static Athlete createAthleteWithNameAndScore(String name, int score) {
        Result result = new Result();
        result.setTotalScore(score);
        Athlete athlete = new Athlete(name);
        athlete.setResult(result);
        return athlete;
    }

}
