package com.yasemin.decathlon.bussiness;

import com.yasemin.decathlon.model.Athlete;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.yasemin.decathlon.data.AthleteCreator.createAthleteWithNameAndScore;
import static org.junit.Assert.assertEquals;


public class PositionCalculationTest {

    private final PositionCalculation positionCalculation = new PositionCalculation();
    private List<Athlete> athleteList = new ArrayList<>();

    @Test
    public void shouldCalculatePositionsForOneAthlete() {
        // given
        Athlete athlete = createAthleteWithNameAndScore("Barney", 4000);
        athleteList.add(athlete);

        // when
        positionCalculation.calculatePositions(athleteList);

        // then
        assertEquals(athlete.getPosition(), "1");
    }

    @Test
    public void shouldCalculateForSamePositions() {
        // given
        Athlete yasemin = createAthleteWithNameAndScore("Yasemin", 4200);
        Athlete penny = createAthleteWithNameAndScore("Penny", 3500);
        Athlete david = createAthleteWithNameAndScore("David", 3500);
        Athlete alex = createAthleteWithNameAndScore("Alex", 3500);
        athleteList.addAll(Arrays.asList(yasemin, penny, david, alex));

        // when
        positionCalculation.calculatePositions(athleteList);

        // then
        assertEquals(yasemin.getPosition(), "1");
        assertEquals(penny.getPosition(), "2-3-4");
        assertEquals(alex.getPosition(), "2-3-4");
        assertEquals(david.getPosition(), "2-3-4");
    }

    @Test
    public void shouldSortByNameIfTotalScoresEqual() {
        //given
        int score = 3000;
        Athlete michael = createAthleteWithNameAndScore("Michael", score);
        Athlete rachael = createAthleteWithNameAndScore("Rachael", score);
        Athlete lily = createAthleteWithNameAndScore("Lily", score);
        athleteList.addAll(Arrays.asList(michael, rachael, lily));

        positionCalculation.calculatePositions(athleteList);

        assertEquals(athleteList.get(0), lily);
        assertEquals(athleteList.get(1), michael);
        assertEquals(athleteList.get(2), rachael);
    }



}