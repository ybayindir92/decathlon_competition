package com.yasemin.decathlon.bussiness;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static com.yasemin.decathlon.model.Event.*;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class FieldEventCalculationTest {

    private final EventCalculation fieldCalculation = new FieldEventCalculation();
    private float performance;
    private int expectedScore;

    @Test
    public void shouldCalculateDiscussThrow() {
        // Athlete:Jürgen Schult, Performance: 74.08, Score:1383, Date:6 June 1986, Location:Neubrandenburg
        performance = 74.08f;
        expectedScore = 1383;

        // when
        int actualResult = fieldCalculation.calculateScore(DISCUS_THROW, performance);

        //then
        assertEquals(expectedScore, actualResult);
    }

    @Test
    public void shouldCalculateHighJump() {
        // Athlete:Javier Sotomayor, Performance: 2.45, Score:1244, Date:27 July 1993, Location:Salamanca
        performance = 2.45f;
        expectedScore = 1244;

        // when
        int actualResult = fieldCalculation.calculateScore(HIGH_JUMP, performance);

        // then
        assertEquals(expectedScore, actualResult);
    }

    @Test
    public void shouldCalculateLongJump() {
        // Athlete:Mike Powell, Performance: 8.95, Score:1312, Date:30 August 1991, Location:Tokyo
        performance = 8.95f;
        expectedScore = 1312;

        // when
        int actualResult = fieldCalculation.calculateScore(LONG_JUMP, performance);

        // then
        assertEquals(expectedScore, actualResult);
    }

    @Test
    public void shouldCalculateJavelinThrow() {
        // Athlete:Jan Zelezny, Performance: 98.48, Score:1331, Date:25 May 1996, Location:Jena
        performance = 98.48f;
        expectedScore = 1331;

        // when
        int actualResult = fieldCalculation.calculateScore(JAVELIN_THROW, performance);

        // then
        assertEquals(expectedScore, actualResult);
    }

    @Test
    public void shouldCalculatePoleVault() {
        // Athlete:Renaud Lavillenie, Performance: 6.16, Score:1284, Date:30 August 1991, Location:Donetsk
        performance = 6.16f;
        expectedScore = 1284;

        // when
        int actualResult = fieldCalculation.calculateScore(POLE_VAULT, performance);

        // then
        assertEquals(expectedScore, actualResult);
    }

    @Test
    public void shouldCalculateShotPut() {
        // Athlete:Randy Barnes, Performance: 23.12, Score:1295, Date:20 May 1990, Location:Westwood
        performance = 23.12f;
        expectedScore = 1295;

        // when
        int actualResult = fieldCalculation.calculateScore(SHOT_PUT, performance);

        // then
        assertEquals(expectedScore, actualResult);
    }
}