package com.yasemin.decathlon.bussiness;

import com.yasemin.decathlon.model.Event;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static com.yasemin.decathlon.model.Event.SPRINT_100M;
import static com.yasemin.decathlon.model.Event.SPRINT_400M;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TrackEventCalculationTest {

    final private EventCalculation trackCalculation = new TrackEventCalculation();
    private float performance;
    private int expectedScore;

    @Test
    public void shouldCalculateSprint100m() {
        // Athlete:Usain Bolt, Performance: 9.58, Score:1202, Date:16 August 2009, Location:Berlin
        performance = 9.58f;
        expectedScore = 1202;

        // when
        int actualResult = trackCalculation.calculateScore(SPRINT_100M, performance);

        // then
        assertEquals(expectedScore, actualResult);
    }

    @Test
    public void shouldCalculateSprint400m() {
        // Athlete:Wayde van Niekerk, Performance: 43.03, Score:1164, Date:14 August 2016, Location:Rio de Janeiro
        performance = 43.03f;
        expectedScore = 1164;

        // when
        int actualResult = trackCalculation.calculateScore(SPRINT_400M, performance);

        // then
        assertEquals(expectedScore, actualResult);

    }

    @Test
    public void shouldCalculateHurdles110m() {
        // Athlete:Aries Merritt, Performance: 12.80, Score:1135, Date:7 September 2012, Location:Brussels
        performance = 12.80f;
        expectedScore = 1135;

        // when
        int actualResult = trackCalculation.calculateScore(Event.HURDLES_110M, performance);

        // then
        assertEquals(expectedScore, actualResult);
    }

    @Test
    public void shouldCalculateSprint1500m() {
        // Athlete:Hicham El Guerrouj, Performance: 3 m 26.00 , Score:1218, Date:14 July 1998, Location:Rome
        performance = 206.00f;
        expectedScore = 1218;

        // when
        int actualResult = trackCalculation.calculateScore(Event.SPRINT_1500M, performance);

        // then
        assertEquals(expectedScore, actualResult);
    }
}