package com.yasemin.decathlon.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParserUtilityTest {

    @Test
    public void shouldParseRegularString() {
        // when
        float actual = ParserUtility.parseFromString("1.5");

        // then
        assertEquals(actual,1.5f,0.03);
    }

    @Test
    public void shouldParseMinuteSecondString() {
        // when
        float actual = ParserUtility.parseFromString("3.15.12");

        // then
        assertEquals(actual,195.15f,0.03);
    }

    @Test(expected = NumberFormatException.class)
    public void shouldNotParseDifferentFormatString() {
        // when
        ParserUtility.parseFromString("2,15:12");
    }
}