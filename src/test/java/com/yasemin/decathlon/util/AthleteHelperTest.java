package com.yasemin.decathlon.util;

import com.yasemin.decathlon.model.Athlete;
import org.junit.Test;

import static com.yasemin.decathlon.model.Event.SPRINT_100M;
import static org.junit.Assert.assertEquals;

public class AthleteHelperTest {

    @Test
    public void shouldCreateAthleteObjectFromArray() {
        // when
        Athlete athlete = AthleteHelper.createAthlete(new String[]
                {"Alex", "13.75", "4.84", "10.12", "1.50", "68.44", "19.18", "30.85", "2.80", "33.88", "6.22.75"});

        // then
        assertEquals(athlete.getName(), "Alex");
        assertEquals(athlete.getResult().getEventResult(SPRINT_100M), 13.75f, 0f);
    }
}