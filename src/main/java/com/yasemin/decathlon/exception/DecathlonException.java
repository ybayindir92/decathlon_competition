package com.yasemin.decathlon.exception;

/**
 * Decathlon project logical exceptions
 */
public class DecathlonException extends RuntimeException {

    public DecathlonException(String message) {
        super(new StringBuilder("Decathlon exception: ").append(message).toString());
    }
}
