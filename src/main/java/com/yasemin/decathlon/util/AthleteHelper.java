package com.yasemin.decathlon.util;

import com.yasemin.decathlon.exception.DecathlonException;
import com.yasemin.decathlon.model.Athlete;
import com.yasemin.decathlon.model.Event;
import com.yasemin.decathlon.model.Result;

public class AthleteHelper {

    /**
     * Sets all games performances to athlete results.
     * @param athleteResults
     */
    public static Athlete createAthlete(String[] athleteResults) {
        if (athleteResults.length != Event.values().length + 1) {
            throw new DecathlonException("Athlete result array must contain all Decathlon games results!");
        }
        Athlete athlete = new Athlete();
        athlete.setName(athleteResults[0]);
        Result result = new Result();
        for (Event event : Event.values()) {
            result.setEventResult(event, athleteResults[event.ordinal()+1]);
        }
        athlete.setResult(result);
        return athlete;
    }
}
