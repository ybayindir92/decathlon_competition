package com.yasemin.decathlon.util;

import com.yasemin.decathlon.model.DecathlonConfig;

/**
 * In addition to parseFloat method it can convert.<br/>
 * minute.second.millisecond type string format to float.
 */
public class ParserUtility {

    public static float parseFromString(String value) throws NumberFormatException {
        if (value.matches(DecathlonConfig.TIME_FORMAT)) {
            String[] values = value.split(DecathlonConfig.TIME_SEPARATOR);
            String minute = values[0];
            String second = values[1] + "." + values[2];
            return (Float.parseFloat(minute) * 60 + Float.parseFloat(second));
        } else {
            return Float.parseFloat(value);
        }
    }
}
