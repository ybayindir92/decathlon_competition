package com.yasemin.decathlon;

import com.yasemin.decathlon.bussiness.PositionCalculation;
import com.yasemin.decathlon.document.factory.DocumentParserFactory;
import com.yasemin.decathlon.document.factory.DocumentWriterFactory;
import com.yasemin.decathlon.document.parser.DocumentParser;
import com.yasemin.decathlon.document.writer.DocumentWriter;
import com.yasemin.decathlon.model.Athlete;

import java.util.List;

import static com.yasemin.decathlon.model.DecathlonConfig.*;

public class DecathlonMain {

    public static void main(String[] args) {
        try {
            // Get input file path from arguments
            // Otherwise defaults are used
            String inputFileName = DEFAULT_INPUT_FILE;
            String outputFileName = DEFAULT_OUTPUT_FILE;
            if (args != null && args.length > 0) {
                inputFileName = args[0];
                if (args.length > 1) {
                    outputFileName = args[1];
                }
            }

            // Document parsing
            DocumentParser documentParser = DocumentParserFactory.getDocumentParser(CSV_PARSER_IMPLEMENTATION);
            List<Athlete> athletes = documentParser.parseDocument(inputFileName == null ? DEFAULT_INPUT_FILE : inputFileName);

            // Total score calculation
            PositionCalculation positionCalculation = new PositionCalculation();
            positionCalculation.calculatePositions(athletes);
            System.out.println(athletes);

            // Document writing
            DocumentWriter<Athlete> documentWriter = DocumentWriterFactory.getDocumentWriter(XML_WRITER_IMPLEMENTATION);
            documentWriter.write(athletes, outputFileName);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
