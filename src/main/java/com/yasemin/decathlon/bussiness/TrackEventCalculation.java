package com.yasemin.decathlon.bussiness;

import com.yasemin.decathlon.model.Event;

public class TrackEventCalculation implements EventCalculation {

    /**
     * Calculates points of track events with 'Points = INT(A(B — P)^C)' formula
     * @param event
     * @param performance
     * @return
     */
    public int calculateScore(final Event event, final float performance) {
        if (performance == 0) {
            return 0;
        }
        double difference = event.getColumnB() - performance * event.getUnitConstant();
        double powerOfDiff = Math.pow(difference, event.getColumnC());
        return (int) (event.getColumnA() * powerOfDiff);
    }
}

