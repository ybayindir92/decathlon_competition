package com.yasemin.decathlon.bussiness;

import com.yasemin.decathlon.model.Event;

public interface EventCalculation {

    /**
     * Common calculations method of Decathlon Competition
     * @param event
     * @param performance
     * @return
     */
    int calculateScore(final Event event,final float performance);
}
