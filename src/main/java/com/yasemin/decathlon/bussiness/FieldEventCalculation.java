package com.yasemin.decathlon.bussiness;

import com.yasemin.decathlon.model.Event;

public class FieldEventCalculation implements EventCalculation {

    /**
     * Calculates points of field events with 'Points = INT(A(P — B)^C)' formula
     * @param event
     * @param performance
     * @return
     */
    public int calculateScore(final Event event, final float performance) {
        if (performance == 0) {
            return 0;
        }
        double difference = performance * event.getUnitConstant() - event.getColumnB();
        double powerOfDiff = Math.pow(difference, event.getColumnC());
        return (int)(event.getColumnA() * powerOfDiff);
    }
}
