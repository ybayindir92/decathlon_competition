package com.yasemin.decathlon.bussiness;

import com.yasemin.decathlon.model.Athlete;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PositionCalculation {

    private Map<Integer, Integer> rankingMap;
    private int currentRank;

    public PositionCalculation() {
        this.rankingMap = new LinkedHashMap<>();
        this.currentRank = 1;
    }

    /**
     * Calculates positions of athletes
     * @param athletes
     */
    public void calculatePositions(List<Athlete> athletes) {
        orderAthletes(athletes);
        createRankingMap(athletes);

        for (int occurrence : rankingMap.values()) {
            writePositionLabel(occurrence, athletes);
            currentRank += occurrence;
        }
    }

    /**
     * Sorts athletes based on scores and their names.
     * @param athletes
     */
    private void orderAthletes(List<Athlete> athletes) {
        Collections.sort(athletes);
        Collections.reverse(athletes);
    }

    /**
     * Creates ranking map for total scores occurrence
     * @param athletes
     */
    private void createRankingMap(final List<Athlete> athletes) {
        for (Athlete athlete : athletes) {
            int totalScore = athlete.getResult().getTotalScore();
            if (rankingMap.containsKey(totalScore)) {
                rankingMap.put(totalScore, rankingMap.get(totalScore) + 1);
            } else {
                rankingMap.put(totalScore, 1);
            }
        }
    }

    /**
     * Writes position label for same score groups of athlete
     * @param occurrence
     * @param athletes
     */
    private void writePositionLabel(final int occurrence, final List<Athlete> athletes) {
        StringBuilder placeStr = new StringBuilder(Integer.toString(currentRank));
        for (int i = 1; i < occurrence; i++) {
            placeStr.append("-").append(i + currentRank);
        }
        for (int i = 0; i < occurrence; i++) {
            athletes.get(i + currentRank - 1).setPosition(placeStr.toString());
        }
    }
}
