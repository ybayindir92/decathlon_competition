package com.yasemin.decathlon.model;

import static com.yasemin.decathlon.model.EventType.FIELD;
import static com.yasemin.decathlon.model.EventType.TRACK;

/**
 * Decathlon Competition Events and Point System
 */
public enum Event {
    SPRINT_100M(TRACK, 25.4347f, 18f, 1.81f, 1),
    LONG_JUMP(FIELD, 0.14354f, 220f, 1.4f, 100),
    SHOT_PUT(FIELD, 51.39f, 1.5f, 1.05f, 1),
    HIGH_JUMP(FIELD, 0.8465f, 75f, 1.42f, 100),
    SPRINT_400M(TRACK, 1.53775f, 82f, 1.81f, 1),
    HURDLES_110M(TRACK, 5.74352f, 28.5f, 1.92f, 1),
    DISCUS_THROW(FIELD, 12.91f, 4f, 1.1f, 1),
    POLE_VAULT(FIELD, 0.2797f, 100f, 1.35f, 100),
    JAVELIN_THROW(FIELD, 10.14f, 7f, 1.08f, 1),
    SPRINT_1500M(TRACK, 0.03768f, 480f, 1.85f, 1);

    private EventType eventType;
    private float columnA;
    private float columnB;
    private float columnC;
    private int unitConstant;//Same events need unit conversion.

    Event(EventType eventType, float columnA, float columnB, float columnC, int unitConstant) {
        this.eventType = eventType;
        this.columnA = columnA;
        this.columnB = columnB;
        this.columnC = columnC;
        this.unitConstant = unitConstant;
    }

    // only getters can be used
    public float getColumnA() {
        return columnA;
    }

    public float getColumnB() {
        return columnB;
    }

    public float getColumnC() {
        return columnC;
    }

    public EventType getEventType() {
        return eventType;
    }

    public int getUnitConstant() {
        return unitConstant;
    }
}
