package com.yasemin.decathlon.model;

import static java.io.File.separator;

public class DecathlonConfig {
    public static final String CSV_COLUMN_SEPARATOR = ";";

    public static final String TIME_SEPARATOR = "\\.";
    public static final String TIME_FORMAT = "\\d+" + TIME_SEPARATOR + "\\d+" + TIME_SEPARATOR + "\\d+";

    public static final String XML_INDENT_PROPERTY = "{http://xml.apache.org/xslt}indent-amount";
    public static final String XML_INDENT_VALUE = "3";
    public static final String XML_ROOT_NAME = "Decathlon";
    public static final String XML_ATHLETE_FIELD = "Athlete";
    public static final String XML_ATHLETE_POSITION = "Position";
    public static final String XML_ATHLETE_NAME = "Name";
    public static final String XML_ATHLETE_SCORE = "Score";
    public static final String XML_ATHLETE_RESULTS = "Results";

    public static final String DEFAULT_INPUT_FILE = "sample" + separator + "results.csv";
    public static final String DEFAULT_OUTPUT_FILE = "output.xml";

    public static final String CSV_PARSER_IMPLEMENTATION = "CSV";
    public static final String XML_WRITER_IMPLEMENTATION = "XML";
}
