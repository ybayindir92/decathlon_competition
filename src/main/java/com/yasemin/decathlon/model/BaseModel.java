package com.yasemin.decathlon.model;

/**
 * Parent of all model classes
 */
public abstract class BaseModel {
    // Created for building a general structure for models
    // It can be contain some common fields in the future
}
