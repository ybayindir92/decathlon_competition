package com.yasemin.decathlon.model;

/**
 * Decathlon Competition Event Types
 */
public enum EventType {
    TRACK,
    FIELD
}
