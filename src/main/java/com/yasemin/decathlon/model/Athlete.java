package com.yasemin.decathlon.model;

public class Athlete extends BaseModel implements Comparable<Athlete> {
    private String name;
    private Result result;
    private String position;

    public Athlete() {
        this("NA");
    }

    public Athlete(String name) {
        this.name = name;
        this.position = "NA";
    }

    public Athlete(String name, Result result) {
        this.name = name;
        this.result = result;
    }

    public Athlete(String name, String position) {
        this.name = name;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * Compares two athletes based on the score<br/>
     * If this athlete's score is higher, then returns positive otherwise negative <br/>
     * If both athletes have same score, then sort by name <br/>
     * If scores and names are equal, then returns zero.
     */
    @Override
    public int compareTo(Athlete athlete) {
        int thisScore = this.getResult().getTotalScore();
        int otherScore = athlete.getResult().getTotalScore();
        if (thisScore == otherScore) {
            return athlete.getName().compareTo(this.getName());
        } else {
            return thisScore - otherScore;
        }
    }

    @Override
    public String toString() {
        return "Name:" + this.name + " Score:" + result.getTotalScore() + " Rank:" + this.position;
    }
}
