package com.yasemin.decathlon.model;

import com.yasemin.decathlon.bussiness.EventCalculation;
import com.yasemin.decathlon.bussiness.FieldEventCalculation;
import com.yasemin.decathlon.bussiness.TrackEventCalculation;

import java.util.HashMap;
import java.util.Map;

import static com.yasemin.decathlon.model.EventType.FIELD;
import static com.yasemin.decathlon.util.ParserUtility.parseFromString;

public class Result extends BaseModel {

    private Map<Event, Float> resultMap;
    private int totalScore = 0;
    private EventCalculation fieldEventCalculation;
    private EventCalculation trackEventCalculation;

    public Result() {
        this(new TrackEventCalculation(), new FieldEventCalculation());
    }

    private Result(TrackEventCalculation track, FieldEventCalculation field) {
        this.trackEventCalculation = track;
        this.fieldEventCalculation = field;
        init();
    }

    public int getTotalScore() {
        if (this.totalScore == 0) {
            calculateTotalScore();
        }
        return this.totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public void setEventResult(Event event, String performance) {
        resultMap.put(event, parseFromString(performance));
    }

    public float getEventResult(Event event) {
        return resultMap.getOrDefault(event, 0f);
    }


    /**
     * Sets zero for all games performance.
     */
    private void init() {
        resultMap = new HashMap<>();
        for (Event event : Event.values()) {
            resultMap.put(event, 0.0f);
        }
    }

    /**
     * Calculates total score for Athletes.<br/>
     * If game is a field event, uses field event formula for calculation.<br/>
     * If game is a track event, uses track event formula for calculation.
     */
    private void calculateTotalScore() {

        for (Event event : Event.values()) {
            if (event.getEventType() == FIELD) {
                totalScore += fieldEventCalculation.calculateScore(event, resultMap.get(event));
            } else {
                totalScore += trackEventCalculation.calculateScore(event, resultMap.get(event));
            }
        }
    }
}
