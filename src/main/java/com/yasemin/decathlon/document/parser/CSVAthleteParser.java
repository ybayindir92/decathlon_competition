package com.yasemin.decathlon.document.parser;

import com.yasemin.decathlon.model.Athlete;
import com.yasemin.decathlon.model.DecathlonConfig;
import com.yasemin.decathlon.util.AthleteHelper;

import java.util.ArrayList;
import java.util.List;

public class CSVAthleteParser extends CSVParser<Athlete> {

    /**
     * Reads data from given file path <br/>
     * Returns Athlete list.
     * @param
     * @return
     */
    @Override
    protected List<Athlete> parseDocumentToList(List<String> dataList) {
        List<Athlete> athletes = new ArrayList<>();
        for (String athleteResultLine : dataList) {
            String[] athleteResult = athleteResultLine.split(DecathlonConfig.CSV_COLUMN_SEPARATOR);
            athletes.add(AthleteHelper.createAthlete(athleteResult));
        }
        return athletes;
    }
}
