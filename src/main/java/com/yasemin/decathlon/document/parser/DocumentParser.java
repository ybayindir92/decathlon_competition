package com.yasemin.decathlon.document.parser;

import com.yasemin.decathlon.model.BaseModel;

import java.io.IOException;
import java.util.List;

public interface DocumentParser<T extends BaseModel> {

    /**
     * Common method for parsing given file.
     * @param filename
     * @return
     * @throws IOException
     * @throws IllegalArgumentException
     */
    List<T> parseDocument(String filename) throws IOException;
}
