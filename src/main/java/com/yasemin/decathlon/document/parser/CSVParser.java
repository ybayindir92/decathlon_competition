package com.yasemin.decathlon.document.parser;

import com.yasemin.decathlon.model.BaseModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class CSVParser<I extends BaseModel> implements DocumentParser<I> {

    /**
     * Opens given file and returns 'parseDocumentToList' method.
     * @param filename
     * @return
     * @throws IOException
     */
    @Override
    public final List<I> parseDocument(String filename) throws IOException {
        List<String> list = new ArrayList<>();

        final File file = new File(filename);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null && !line.isEmpty()) {
            list.add(line.trim());
        }
        System.out.println("Input file is read from: " + file.getAbsolutePath());
        br.close();
        return parseDocumentToList(list);
    }

    /**
     * Reads data from given file path <br/>
     * Returns list.
     * @param dataList
     * @return
     */
    protected abstract List<I> parseDocumentToList(List<String> dataList);
}
