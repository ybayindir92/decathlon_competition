package com.yasemin.decathlon.document.writer;

import com.yasemin.decathlon.model.BaseModel;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.util.List;

public interface DocumentWriter<T extends BaseModel> {

    /**
     * Common method for writing a list to document
     * @param list
     * @param fileName
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    void write(List<T> list, String fileName) throws ParserConfigurationException,TransformerException;
}
