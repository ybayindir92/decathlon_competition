package com.yasemin.decathlon.document.writer;

import com.yasemin.decathlon.model.Athlete;
import com.yasemin.decathlon.model.DecathlonConfig;
import com.yasemin.decathlon.model.Event;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.List;

public class XMLAthleteWriter extends XMLWriter<Athlete> {

    /**
     * Adds content to Xml file
     * @param doc
     * @param athleteList
     */
    @Override
    public void createContent(Document doc, List<Athlete> athleteList) {
        Element rootElement = doc.createElement(DecathlonConfig.XML_ROOT_NAME);
        doc.appendChild(rootElement);

        for (Athlete athlete : athleteList) {
            Element athleteElement = doc.createElement(DecathlonConfig.XML_ATHLETE_FIELD);
            rootElement.appendChild(athleteElement);

            addElement(doc, athleteElement, DecathlonConfig.XML_ATHLETE_POSITION, athlete.getPosition());
            addElement(doc, athleteElement, DecathlonConfig.XML_ATHLETE_NAME, athlete.getName());
            addElement(doc, athleteElement, DecathlonConfig.XML_ATHLETE_SCORE, "" + athlete.getResult().getTotalScore());

            Element resultsElement = doc.createElement(DecathlonConfig.XML_ATHLETE_RESULTS);
            athleteElement.appendChild(resultsElement);

            for (Event event : Event.values()) {
                String eventResult = String.valueOf(athlete.getResult().getEventResult(event));
                addElement(doc, resultsElement, event.name(), eventResult);
            }
        }
    }
}
