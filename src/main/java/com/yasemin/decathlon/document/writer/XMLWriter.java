package com.yasemin.decathlon.document.writer;

import com.yasemin.decathlon.exception.DecathlonException;
import com.yasemin.decathlon.model.BaseModel;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

import static com.yasemin.decathlon.model.DecathlonConfig.XML_INDENT_PROPERTY;
import static com.yasemin.decathlon.model.DecathlonConfig.XML_INDENT_VALUE;

public abstract class XMLWriter<O extends BaseModel> implements DocumentWriter<O>{

    /**
     * Writes a list to document
     * @param list
     * @param fileName
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    @Override
    public final void write(List<O> list, String fileName) throws ParserConfigurationException, TransformerException {
        if (list == null) {
            throw new DecathlonException("Athletes should not ve empty");
        }

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        Document doc = docBuilder.newDocument();
        createContent(doc, list);

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(XML_INDENT_PROPERTY, XML_INDENT_VALUE);
        DOMSource sourceDOM = new DOMSource(doc);
        File file = new File(fileName);
        StreamResult resultXML = new StreamResult(file);
        transformer.transform(sourceDOM, resultXML);
        System.out.println("Output file is written to: " + file.getAbsolutePath());
    }

    /**
     * Adds element to XML document.
     * @param doc
     * @param parentElement
     * @param elementName
     * @param value
     */
    void addElement(Document doc, Element parentElement, String elementName, String value) {
        Element element = doc.createElement(elementName);
        element.appendChild(doc.createTextNode(value));
        parentElement.appendChild(element);
    }

    /**
     * Responsible for create Xml content
     * @param doc
     * @param list
     */
    public abstract void createContent(Document doc, List<O> list);
}
