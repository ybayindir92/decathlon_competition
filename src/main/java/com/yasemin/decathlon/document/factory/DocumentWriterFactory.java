package com.yasemin.decathlon.document.factory;

import com.yasemin.decathlon.document.writer.DocumentWriter;
import com.yasemin.decathlon.document.writer.XMLAthleteWriter;
import com.yasemin.decathlon.exception.DecathlonException;

import static com.yasemin.decathlon.model.DecathlonConfig.XML_WRITER_IMPLEMENTATION;

public class DocumentWriterFactory {

    /**
     * Returns desired document writer based on file type
     * @param type
     * @return
     */
    public static DocumentWriter getDocumentWriter(String type) {
        if (XML_WRITER_IMPLEMENTATION.equals(type)) {
            return new XMLAthleteWriter();
        }
        throw new DecathlonException(type + "type is not implemented yet.");
    }
}
