package com.yasemin.decathlon.document.factory;

import com.yasemin.decathlon.document.parser.CSVAthleteParser;
import com.yasemin.decathlon.document.parser.DocumentParser;
import com.yasemin.decathlon.exception.DecathlonException;

import static com.yasemin.decathlon.model.DecathlonConfig.CSV_PARSER_IMPLEMENTATION;

public class DocumentParserFactory {

    /**
     * Returns desired document parser based on file type
     * @param type
     * @return
     */
    public static DocumentParser getDocumentParser(String type) {
        if (CSV_PARSER_IMPLEMENTATION.equals(type)) {
            return new CSVAthleteParser();
        }
        throw new DecathlonException(type + "type is not implemented yet.");
    }
}
